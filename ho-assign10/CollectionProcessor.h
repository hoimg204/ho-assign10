//
//  CollectionProcessor.h
//  ho-assign10
//
//  Created by Henrique de Oliveira Carvalho on 2014-11-18.
//  Copyright (c) 2014 beta. All rights reserved.
//
//  Problem Statement:
//  Sort the array containing the generated data in reverse alphabetical
//  order according to first name.
//  Sort the array containing the generated data in reverse alphabetical
//  order according to the last character of the last name.
//  Sort the array containing the generated data according to the birth
//  year.
//  Use the array containing the generated data to display only records
//  belonging to full time students.
//  Use the dictionary to find out how many records are describing persons
//  from each city.
//  Use the dictionary to find how many unique last names were generated.
//  Use the set to find out number of unique last names.
//  Use the set to find out number of unique cities.
//
//  INPUTS:    none
//  OUTPUTS:   none
//
// ************************************************************************

#import <Foundation/Foundation.h>
#import "Globals.h"
#import "ConsoleLine.h"
#import "Person.h"

@interface CollectionProcessor : NSObject

@property NSMutableArray * array;
@property NSMutableDictionary * dictionary;
@property NSMutableSet * set;

- (id) initWithData:(NSMutableArray *)data;

- (void) showArray;
- (void) showDictionary;
- (void) showSet;

- (void) showArraySize;
- (void) showDictionarySize;
- (void) showSetSize;

// Your ASSIGNMENT STARTS HERE
- (void) initializeDictionaryByFirstName;
- (void) initializeDictionaryByLastName;
- (void) initializeDictionaryByCity;
- (void) initializeSetWithFirstNames;

- (void) sortArrayByFirstName;
- (void) sortArrayByBirthYear;
- (void) sortArrayByLastCharOfLastName;

- (void) showArrayWithFullTimeStudents;
- (void) showHowManyRecordPersonByCity;
- (void) showHowManyUniqueLastNameDictionary;
- (void) showHowManyUniqueLastNameSet;
- (void) showHowManyUniqueCitiesSet;

@end
