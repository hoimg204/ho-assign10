//
//  DataProcessor.m
//  Dictionary
//
//  Created by Frank  Niscak on 2014-11-04.
//  Copyright (c) 2014 NIC. All rights reserved.
//

#import "DataProcessor.h"
#import "Globals.h"
#import "ConsoleLine.h"
#import "Generator.h"
#import "Person.h"

@implementation DataProcessor
{
    NSMutableArray * people;
}

/////////////////////////////////////////////////////////////////////////////
// Generate Person records into "problem9/people.txt" file
/////////////////////////////////////////////////////////////////////////////
- (void) generateRecords:(int)howMany
{
    ConsoleLine * console = [[ConsoleLine alloc] init];
    const int DEFAULT_SIZE = 2;
    
    // generate Persons into an array
    Generator * generator = [[Generator alloc] init];
    
    if (howMany <= 0)
    {
        howMany = DEFAULT_SIZE;
    }
    NSArray * persons = [generator generateMultiplePersonRecords:howMany];
    
    // NSLog(@"%@", persons);
    
    // Create a single NSString from NSArray persons
    NSString * strPersons = @"";
    for (int pos = 0; pos < [persons count]; pos++)
    {
        strPersons = [strPersons stringByAppendingString:persons[pos]];
        if (pos < [persons count])
        {
            strPersons = [strPersons stringByAppendingString:@"\n"];
        }
    }
    
    // Write to file
    NSError * error = nil;
    NSString * fileName = [console getCompleteFileNameFromFolder:@"problem9" andFileName:@"people.txt"];
    BOOL ok = [strPersons writeToFile:fileName atomically:YES encoding:NSASCIIStringEncoding error:&error];
    
    if ( ok != YES)
    {
        NSLog(@"Failed to write to the file");
    }
}

/////////////////////////////////////////////////////////////////////////////
// read all records from "problem9/people.txt" into "people" NSMutableArray
/////////////////////////////////////////////////////////////////////////////
- (void) readAllRecords
{
    ConsoleLine * console = [[ConsoleLine alloc] init];
    if (people != nil)
        [people removeAllObjects];
    else
        people = [NSMutableArray array];
    
    // Read the file
    NSArray * lines = [console getNSArrayOfLinesFromFolder:@"problem9" andFilename:@"people.txt"];
    
    //NSLog(@"%@", lines);
    
    
    // split lines into words by using | and display words (one per line)
    // and create an NSMutableArray of Persons
    for (int pos = 0; pos < [lines count] - 1; pos++)
    {
        Person * person = [[Person alloc] init];
        // NSLog((@"%@", lines[pos]));
        NSArray * words = [lines[pos] componentsSeparatedByString:@"|"];
        for (int index = 0; index < [words count]; index++)
        {
            switch (index)
            {
                case 1:
                    [person setFirstName:words[index]];
                    break;
                case 2:
                    [person setLastName:words[index]];
                    break;
                case 3:
                    [person setBirthYear:[words[index] intValue]];
                    break;
                case 4:
                    [person setStreet:words[index]];
                    break;
                case 5:
                    [person setStreetNumber:[words[index] intValue]];
                    break;
                case 6:
                    [person setCity:words[index]];
                    break;
                case 7:
                    [person setProvince:words[index]];
                    break;
                case 8:
                    [person setProfession:words[index]];
                    break;
            }
            // NSLog(@"%@", words[index]);
        }
        [people addObject:person];
    }
}

-(NSMutableArray *) getArrayOfPersons
{
    if (people == nil)
        people = [NSMutableArray array];
    
    return people;
}

@end
