//
//  DataProcessor.h
//  Dictionary
//
//  Created by Frank  Niscak on 2014-11-04.
//  Copyright (c) 2014 NIC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataProcessor : NSObject

-(void) generateRecords:(int) howMany;
-(void) readAllRecords;
-(NSMutableArray *) getArrayOfPersons;

@end
