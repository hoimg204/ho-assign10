//
//  Globals.h
//  Dictionary
//
//  Created by Frank  Niscak on 2014-10-28.
//  Copyright (c) 2014 NIC. All rights reserved.
//

#ifndef Dictionary_Globals_h
#define Dictionary_Globals_h

#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);

#endif