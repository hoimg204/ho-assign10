//
//  CollectionProcessor.m
//  ho-assign10
//
//  Created by Henrique de Oliveira Carvalho on 2014-11-18.
//  Copyright (c) 2014 beta. All rights reserved.
//
//  Problem Statement:
//  Sort the array containing the generated data in reverse alphabetical
//  order according to first name.
//  Sort the array containing the generated data in reverse alphabetical
//  order according to the last character of the last name.
//  Sort the array containing the generated data according to the birth
//  year.
//  Use the array containing the generated data to display only records
//  belonging to full time students.
//  Use the dictionary to find out how many records are describing persons
//  from each city.
//  Use the dictionary to find how many unique last names were generated.
//  Use the set to find out number of unique last names.
//  Use the set to find out number of unique cities.
//
//  INPUTS:    none
//  OUTPUTS:   none
//
// ******************************************************************************

#import "CollectionProcessor.h"

@implementation CollectionProcessor

- (id) initWithData:(NSMutableArray *)data
{
  self = [super init];

  if (self)
  {
    _array = [data copy];
    _dictionary = [[NSMutableDictionary alloc] init];
    _set = [[NSMutableSet alloc] init];
  }

  return self;
}

- (void) showArraySize
{
  NSLog(@"There are %lu persons in your Array", (unsigned long)
        [self.array count]);
}

- (void) showDictionarySize
{
  NSLog(@"There are %lu persons in your Dictionary", (unsigned long)
        [self.dictionary count]);
}

- (void) showSetSize
{
  NSLog(@"There are %lu persons in your Set", (unsigned long)
        [self.set count]);
}

- (void) showArray
{
  for (Person * human in self.array)
  {
    NSLog(@"%@", human);
  }
  NSLog(@"\n ");
}

- (void) showDictionary
{
  for (id key in _dictionary)
  {
    NSLog(@"Dictionary Key: %@   Value: %@' ", key, _dictionary[key]);
  }
  NSLog(@"\n ");
}

- (void) showSet
{
  for (id item in _set)
  {
    NSLog(@"Set Item:  %@ ", item);
  }
  NSLog(@"\n ");

}

// ******************************************************************************
// Your ASSIGNMENT STARTS HERE

- (void) initializeDictionaryByFirstName
{
  for (Person * human in self.array)
  {
    _dictionary[human.firstName] = human;
  }
}

- (void) initializeDictionaryByLastName
{
  for (Person * human in self.array)
  {
    _dictionary[human.lastName] = human;
  }
}

- (void) initializeDictionaryByCity
{
  for (Person * human in self.array)
  {
    _dictionary[human.city] = [NSNumber numberWithInt:0];
  }
}

- (void) initializeSetWithFirstNames
{
  for (Person * human in self.array)
  {
    [ _set addObject:human.firstName];
  }
}

/* Sort the array containing the generated data in reverse alphabetical order 
 according to first name. */
- (void) sortArrayByFirstName
{

  NSArray * sortedArray =
    [_array sortedArrayUsingComparator:
     ^NSComparisonResult (id obj1, id obj2)
     {
       return [[obj2 firstName] compare:[obj1 firstName] ];
     }

    ];

  _array = (NSMutableArray *) sortedArray;
}

/* Sort the array containing the generated data in reverse alphabetical order 
 according to the last character of the last name. */
- (void) sortArrayByLastCharOfLastName
{

  NSArray * sortedArray =
    [_array sortedArrayUsingComparator:
     ^NSComparisonResult (id obj1, id obj2)
     {
       NSString * lastCharObj1 = [[obj1 lastName] substringFromIndex:
                                  [[obj1 lastName] length] - 1];
       NSString * lastCharObj2 = [[obj2 lastName] substringFromIndex:
                                  [[obj2 lastName] length] - 1];

       return [lastCharObj2 compare:lastCharObj1 ];
     }

    ];

  _array = (NSMutableArray *) sortedArray;
}

/* Sort the array containing the generated data according to the birth year */
- (void) sortArrayByBirthYear
{

  NSArray * sortedArray =
    [_array sortedArrayUsingComparator:
     ^NSComparisonResult (id obj1, id obj2)
     {
       if ([obj1 birthYear]  < [obj2 birthYear] )
       {
         return NSOrderedAscending;
       }
       else if ([obj1 birthYear]  > [obj2 birthYear])
       {
         return NSOrderedDescending;
       }
       else
       {
         return NSOrderedSame;
       }
     }

    ];

  _array = (NSMutableArray *) sortedArray;
}

/* Use the array containing the generated data to display only records 
 belonging to full time students */
- (void) showArrayWithFullTimeStudents
{

  NSString * STR = @"FullTime Student";

  for (Person * human in self.array)
  {
    if ([[human profession] isEqualToString:STR])
    {
      NSLog(@"%@", human);
    }
  }
  NSLog(@"\n ");
}

/* Use the dictionary to find out how many records are describing persons 
 from each city. */
- (void) showHowManyRecordPersonByCity
{
  [_dictionary removeAllObjects];

  [self initializeDictionaryByCity];

  for (Person * human in self.array)
  {
    int counter = 0;

    NSString * nameCity = [human city];

    if ([_dictionary objectForKey:nameCity] != nil)
    {
      counter = [_dictionary[nameCity] intValue] + 1;

      _dictionary[nameCity] = [NSNumber numberWithInt:counter];
    }
  }

  // Display all cities
  for (id key in _dictionary)
  {
    NSLog(@"%@ has %@ persons", key, _dictionary[key]);
  }
  NSLog(@"\n ");
}

/* Use the dictionary to find how many unique last names were generated. */
- (void) showHowManyUniqueLastNameDictionary
{
  [_dictionary removeAllObjects];

  [self initializeDictionaryByLastName];

  NSLog(@"Total number of unique last names: %lu \n", (unsigned long)
        [_dictionary count]);
}

/*Use the set to find out number of unique last names.*/
- (void) showHowManyUniqueLastNameSet
{
  NSMutableSet * lastNames = [[NSMutableSet alloc] init];

  for (Person * human in self.array)
  {
    [lastNames addObject:[human lastName]];
  }

  NSLog(@"Total number of unique last names: %lu \n", (unsigned long)
        [lastNames count]);
}

/*Use the set to find out number of unique cities*/
- (void) showHowManyUniqueCitiesSet
{
  NSMutableSet * cities = [[NSMutableSet alloc] init];

  for (Person * human in self.array)
  {
    [cities addObject:[human city]];
  }

  NSLog(@"Total number of unique cities: %lu \n", (unsigned long)
        [cities count]);
}

@end

