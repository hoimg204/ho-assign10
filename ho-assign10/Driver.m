//
//  Driver.m
//  ho-assign10
//
//  Created by Henrique de Oliveira Carvalho on 2014-11-18.
//  Copyright (c) 2014 beta. All rights reserved.
//
//  Problem Statement:
//  Sort the array containing the generated data in reverse alphabetical
//  order according to first name.
//  Sort the array containing the generated data in reverse alphabetical
//  order according to the last character of the last name.
//  Sort the array containing the generated data according to the birth
//  year.
//  Use the array containing the generated data to display only records
//  belonging to full time students.
//  Use the dictionary to find out how many records are describing persons
//  from each city.
//  Use the dictionary to find how many unique last names were generated.
//  Use the set to find out number of unique last names.
//  Use the set to find out number of unique cities.
//
//  INPUTS:    none
//  OUTPUTS:   none
//
// ******************************************************************************

#import "Driver.h"
#import "Globals.h"
#import "ConsoleLine.h"
#import "Generator.h"
#import "Person.h"
#import "DataProcessor.h"
#import "CollectionProcessor.h"

@implementation Driver

- (void) run
{
  const int SIZE = 15;
  DataProcessor * dataProcessor = [[DataProcessor alloc] init];

  ConsoleLine * console = [[ConsoleLine alloc] init];

  // generate data to a file
  [dataProcessor generateRecords:SIZE ];


  // read data from afile to "dataProcessor" object
  [dataProcessor readAllRecords];

  // get all Persons
  NSMutableArray * people = [dataProcessor getArrayOfPersons];

  CollectionProcessor * processor = [[CollectionProcessor alloc]
                                     initWithData:people];
  [people removeAllObjects];

  // YOUR ASSIGMENT STARTS HERE
  NSLog(@"Press ENTER key to see your Array:");
  [console getConsoleLine];
  [processor showArraySize];
  [processor showArray];

  NSLog(@"Press ENTER key to see your Dictionary:");
  [console getConsoleLine];
  [processor initializeDictionaryByFirstName];
  [processor showDictionarySize];
  [processor showDictionary];

  NSLog(@"Press ENTER key to see your Set:");
  [console getConsoleLine];
  [processor initializeSetWithFirstNames];
  [processor showSetSize];
  [processor showSet];

  /* Sort the array containing the generated data in reverse alphabetical order 
   according to first name. */
  NSLog(@"Press ENTER key to see your Sorted Array:");
  [console getConsoleLine];
  [processor sortArrayByFirstName];
  [processor showArraySize];
  [processor showArray];

  /* Sort the array containing the generated data in reverse alphabetical order 
   according to the last character of the last name.
   */
  NSLog(@"Press ENTER key to see your Sorted Array:");
  [console getConsoleLine];
  [processor sortArrayByLastCharOfLastName];
  [processor showArraySize];
  [processor showArray];

  /*Sort the array containing the generated data according to the birth year.*/
  NSLog(@"Press ENTER key to see your Sorted Array:");
  [console getConsoleLine];
  [processor sortArrayByBirthYear];
  [processor showArraySize];
  [processor showArray];

  /* Use the array containing the generated data to display only records 
   belonging to full time students */
  NSLog(@"Press ENTER key to see your Array:");
  [console getConsoleLine];
  [processor showArrayWithFullTimeStudents];

  /* Use the dictionary to find out how many records are describing persons 
   from each city. */
  NSLog(@"Press ENTER key to see number of persons from each city:");
  [console getConsoleLine];
  [processor showHowManyRecordPersonByCity];

  /*Use the dictionary to find out number of unique last names.*/
  NSLog(@"Press ENTER key to see number of unique last names:");
  [console getConsoleLine];
  [processor showHowManyUniqueLastNameDictionary];

  /*Use the set to find out number of unique last names.*/
  NSLog(@"Press ENTER key to see number of unique last names:");
  [console getConsoleLine];
  [processor showHowManyUniqueLastNameSet];

  /*Use the set to find out number of unique cities*/
  NSLog(@"Press ENTER key to see number of unique cities:");
  [console getConsoleLine];
  [processor showHowManyUniqueCitiesSet];
}

@end

