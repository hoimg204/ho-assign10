//
//  Person.m
//  Dictionary
//
//  Created by Frank  Niscak on 2014-11-01.
//  Copyright (c) 2014 NIC. All rights reserved.
//

#import "Person.h"

@implementation Person

- (NSString *) description
{
    NSString * str = [[NSString alloc] init];
    str = [str stringByAppendingString:self.firstName];
    str = [str stringByAppendingFormat:@"|"];
    str = [str stringByAppendingString:self.lastName];
    str = [str stringByAppendingFormat:@"|"];
    str = [str stringByAppendingFormat:@"%d", self.birthYear];
    str = [str stringByAppendingFormat:@"|"];
    str = [str stringByAppendingString:self.street];
    str = [str stringByAppendingFormat:@"|"];
    str = [str stringByAppendingFormat:@"%d", self.streetNumber];
    str = [str stringByAppendingFormat:@"|"];
    str = [str stringByAppendingString:self.city];
    str = [str stringByAppendingFormat:@"|"];
    str = [str stringByAppendingString:self.province];
    str = [str stringByAppendingFormat:@"|"];
    str = [str stringByAppendingString:self.profession];
    
    return str;
}

@end
