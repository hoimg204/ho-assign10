//
//  main.m
//  ho-assign10
//
//  Created by Henrique de Oliveira Carvalho on 2014-11-18.
//  Copyright (c) 2014 beta. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ConsoleLine.h"
#import "Driver.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        /*
         ConsoleLine *console = [[ConsoleLine alloc] init];
         NSLog(@"Enter a text now : ");
         NSString *line = [console getConsoleLine];
         NSLog(@"You have entered %@", line);
         */
        
        Driver *driver = [[Driver alloc] init];
        [driver run];
        
    }
    return 0;
}

