//
//  Person.h
//  Dictionary
//
//  Created by Frank  Niscak on 2014-11-01.
//  Copyright (c) 2014 NIC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property NSString * firstName;
@property NSString * lastName;
@property int        birthYear;
@property NSString * street;
@property int        streetNumber;
@property NSString * city;
@property NSString * province;
@property NSString * profession;

-(NSString *) description;
@end
