//  Generator.h
//  assign-8-3-2013
//
//  Created by Frank  Niscak on 2014-07-21.
//  Copyright (c) 2014 NIC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Generator : NSObject

- (NSString *) getFirstName;
- (NSString *) getLastName;
- (NSString *) getBirthYear;
- (NSString *) getStreet;
- (NSString *) getStreetNumber;
- (NSString *) getCity;
- (NSString *) getProvince;
- (NSString *) getProfession;
- (NSString *) generatePersonRecord;
- (NSArray *)  generateMultiplePersonRecords: (int) howMany;
- (NSString *) md5: (NSString *) str;
- (NSString *) sha1: (NSString *) str;


@end
