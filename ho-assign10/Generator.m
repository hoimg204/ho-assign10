//
//  Generator.m
//  Dictionary
//
//  Created by Frank  Niscak on 2014-10-27.
//  Copyright (c) 2014 NIC. All rights reserved.
//

//
//  Generator.m
//  assign-8-3-2013
//
//  Created by Frank  Niscak on 2014-07-21.
//  Copyright (c) 2014 NIC. All rights reserved.
//

#import <CommonCrypto/CommonDigest.h>
#import "Generator.h"

@implementation Generator

- (NSString *) getFirstName
{
    NSArray *firstNames = @[@"Alex",
                            @"Ben", @"Cecile",
                            @"Kerna", @"Karel", @"Koula", @"Claudia", @"Ivan",
                            @"Ilone", @"Estheban", @"Thomas", @"Paul", @"Nathan",
                            @"Dan", @"Eva", @"Felix", @"Gomez", @"Hana",
                            @"Ivan"];
    
    NSUInteger count = [firstNames count];
    int rindex = arc4random() % count;
    return firstNames[rindex];
}

- (NSString *) getLastName
{
    NSArray *lastNames = @[@"Atol", @"Bundle",
                           @"Circus",@"Totifol", @"Kraus",
                           @"Bourne", @"Smith", @"Kacer", @"Husa", @"Screen",
                           @"Bourne", @"Smith", @"Kacer", @"Husa", @"Screen",
                           @"Dome", @"Eternal", @"Fantomas", @"Giant",
                           @"Humphrey",
                           @"Immense"];
    
    NSUInteger count = [lastNames count];
    int rindex = arc4random() % count;
    return lastNames[rindex];
}

- (NSString *) getBirthYear
{
    const int COUNT = 20;
    int year = 1980 + arc4random() % COUNT;
    
    NSString *strYear = [NSString stringWithFormat:@"%i", year];
    return strYear;
}

- (NSString *) getStreet
{
    NSArray *streets = @[@"Albatros",
                         @"Buenos",
                         @"Calamity",
                         @"Desolation", @"Ecstatic", @"Fanfare", @"Gologo",
                         @"Terasa", @"Brasil", @"Peru", @"China",
                         @"Rovena", @"Sirupa", @"Zanza", @"Kotato",
                         @"Hovnivar",
                         @"Icarus"];
    
    NSUInteger count = [streets count];
    int rindex = arc4random() % count;
    return streets[rindex];
}

- (NSString *) getStreetNumber
{
    int streetNumber = arc4random() % 5;
    
    NSString *strStreetNumber = [NSString stringWithFormat:@"%i", streetNumber];
    return strStreetNumber;
}

- (NSString *) getCity
{
    NSArray *cities = @[@"Comox", @"Courtenay",
                        @"Cumberland",
                        @"Campbell River", @"Nanaimo", @"Port Hardy",
                        @"River", @"Nanaimo", @"Port Hardy",
                        @"Port Alberni", @"Plosina",
                        @"Victoria"];
    
    NSUInteger count = [cities count];
    int rindex = arc4random() % count;
    return cities[rindex];
}

- (NSString *) getProvince
{
    NSArray *provinces = @[@"AB", @"BC"];
    
    NSUInteger count = [provinces count];
    int rindex = arc4random() % count;
    return provinces[rindex];
}

- (NSString *) getProfession
{
    NSArray *professions = @[@"FullTime Student", @"PartTime Student",
                             @"Instructor",
                             @"PartTime Instructor", @"Administrator", @"Janitor",
                             @"International Student", @"Librarian", @"Office Worker"
                             ];
    
    NSUInteger count = [professions count];
    int rindex = arc4random() % count;
    return professions[rindex];
}

- (NSString *) generatePersonRecord
{
    NSString *firstName = [self getFirstName];
    NSString *lastName = [self getLastName];
    NSString *birthYear = [self getBirthYear];
    NSString *street = [self getStreet];
    NSString *streetNumber = [self getStreetNumber];
    NSString *city = [self getCity];
    NSString *province = [self getProvince];
    NSString *profession = [self getProfession];
    
    // create a combined string to calculate MD5
    NSString *totalString = [[NSString alloc] init];
    totalString = [totalString stringByAppendingString: firstName];
    totalString = [totalString stringByAppendingString: lastName];
    totalString = [totalString stringByAppendingString: birthYear];
    totalString = [totalString stringByAppendingString: street];
    totalString = [totalString stringByAppendingString: streetNumber];
    totalString = [totalString stringByAppendingString: city];
    totalString = [totalString stringByAppendingString: province];
    totalString = [totalString stringByAppendingString: profession];
    
    NSString *md5String = [self md5:totalString];
    
    NSString *strRecord = [NSString stringWithFormat:@"%@|%@|%@|%@|%@|%@|%@|%@|%@", md5String, firstName, lastName, birthYear, street, streetNumber, city, province, profession];
    
    return strRecord;
}

-(NSArray *) generateMultiplePersonRecords: (int) howMany
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for(int pos=0; pos < howMany; pos++)
    {
        [array addObject:[self generatePersonRecord]];
    }
    return array;
}


////////////////////////////////////////////////////////////////////////////
//  MD5 Cryptographic hash
////////////////////////////////////////////////////////////////////////////
- (NSString *) md5:(NSString *) input
{
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
    
}

-(NSString*) sha1:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
    
}




@end
